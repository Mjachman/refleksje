﻿using Ninject.Modules;
using TextParser;

namespace VigenereCaseParser
{
    public class VigenereModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IParser>().To<VigenereCaseParser>();
        }
    }
}
