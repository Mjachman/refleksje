﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TextParser.AbstractParsers
{
    public abstract class  GaderypolukiTypeParsers : CharParser
    {
        private readonly string _code;
        private readonly Dictionary<char, char> _dictionary;

        protected GaderypolukiTypeParsers(string code)
        {
            _code = code;
            _dictionary = CreateDictionairy(code);
        }

        protected override Task<char> ParseCharAsync(char input)
        {
            char res;
            return Task.FromResult(_dictionary.TryGetValue(char.ToUpper(input), out res) 
                ? res 
                : char.ToUpper(input));
        }

        private static Dictionary<char,char> CreateDictionairy(string code)
        {
            var keys = code.Split('-');
            var dic = new Dictionary<char, char>();
            foreach (var k in keys)
            {
                dic.Add(k[0], k[1]);
                dic.Add(char.ToLower(k[0]), char.ToLower(k[1]));

                dic.Add(k[1], k[0]);
                dic.Add(char.ToLower(k[1]), char.ToLower(k[0]));
            }
            return dic;
        }

        public override string ToString()
        {
            return _code;
        }
    }
}
