﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTestAssembly.Folder
{
    public class TestClass2 : TestClass1, Interface1
    {
        public override event EventHandler Event2 { add { } remove { } }
        public override event EventHandler<EventArgs> Event4;
        public new event EventHandler<EventArgs> Event3;

        public override string ToString() =>  "someStuff";

        public override void Method2(IEnumerable<Lazy<Task<double>>> x)
        {
        }

        protected internal override StringBuilder Property5
        {
            get => null;
            set { }
        }

        public new double Method1(int a, DateTime b) => 4.2;

        public Enum1 Method5(Enum1 a)
        {
            return Enum1.A;
        }

        public static TestClass2 operator+ (TestClass2 t, TestClass2 t2)
        {
            return new TestClass2();
        }
    }
}
