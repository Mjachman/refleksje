﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTestAssembly
{
    public abstract class TestClass1
    {
        public int Field1;
        private string Field2;
        protected IEnumerable<double> Field3;
        internal DateTime Field4;
        protected internal StringBuilder Field5;

        public int Property1 {get; protected set;}
        private  string Property2 {get;}
        protected  IEnumerable<double> Property3 {set{}}
        internal DateTime Property4 {get; set;}
        protected internal abstract StringBuilder Property5 {get; set;}

        public int Method1(int a, DateTime b) => 0;
        public abstract void Method2(IEnumerable<Lazy<Task<double>>> x);
        protected virtual IEnumerable<short> Method3(double x)
        {
            yield return 17;
        }
        public Lazy<string> Method4() { return new Lazy<string>(); }

        public event EventHandler Event;
        public abstract event EventHandler Event2;
        public virtual event EventHandler<int> Event3;
        public abstract event EventHandler<EventArgs> Event4;
    }
}
